﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ScrapySharp.Extensions;
using HtmlAgilityPack;
using System.Net.Http;
using System.Dynamic;
using AC5_SUNAT_RazSoc.Models;

namespace AC5_SUNAT_RazSoc.Controllers
{
	[Route("~/api/sunat")]
	public class ApiSUNATController : Controller
	{
		[HttpGet("[action]")]
		public async Task<IActionResult> ConsultarRazonSocial(string filtro)
		{
			dynamic objE = new ExpandoObject();
			objE.success = false;
			objE.data = new List<ContribuyenteEN>();
			try
			{
				var URL = $"https://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?accion=consPorRazonSoc&razSoc={filtro}";
				var client = new HttpClient();
				var response = await client.GetAsync(URL);
				if (response.IsSuccessStatusCode)
				{
					var DATA = await response.Content.ReadAsStringAsync();
					var doc = new HtmlDocument();
					doc.LoadHtml(DATA);
					HtmlNodeCollection lstA = doc.DocumentNode.SelectNodes("//div[@class='list-group']/a");
					if (lstA != null)
					{						
						foreach (HtmlNode item in lstA)
						{
							List<HtmlNode> h4 = new List<HtmlNode>(item.CssSelect("h4"));
							List<HtmlNode> p = new List<HtmlNode>(item.CssSelect("p"));
							objE.data.Add(new ContribuyenteEN()
							{
								ruc = h4[0].InnerText.Split(":")[1].Trim(),
								razon_social = h4[1].InnerText,
								ubicacion = p[0].InnerText,
								estado = p[1].InnerText,
							});
						}
						objE.success = true;
					}
					else
					{
						objE.mensaje = "Por favor, vuelva a consultar";
					}
				}
			}
			catch (Exception ex)
			{

				objE.mensaje = ex.Message;
			}
			return Json(objE);
		}

		[HttpGet("[action]")]
		public async Task<IActionResult> ConsultarRUC(string RUC)
		{
			var objE = Newtonsoft.Json.JsonConvert.DeserializeObject(await SunatMVBStandard.SunatDA.ConsultaSUNAT(RUC));
			return Json(objE);
		}
	}
}