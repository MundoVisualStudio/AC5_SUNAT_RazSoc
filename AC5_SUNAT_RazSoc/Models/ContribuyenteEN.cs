﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AC5_SUNAT_RazSoc.Models
{
    public class ContribuyenteEN
    {
		public string ruc { get; set; }
		public string razon_social { get; set; }
		public string ubicacion { get; set; }
		public string estado { get; set; }
	}
}
