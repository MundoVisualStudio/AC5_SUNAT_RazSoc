﻿class UI {

	static isPlainObject(p) { return Object.prototype.toString.call(p) === '[object Object]' }
	static isEmptyObject(p) { return JSON.stringify(p) === '{}' }

	static Empty(p) {
		var result = false;
		if (p === "" || p === null || p === undefined) {
			result = true;
		}
		if (UI.isPlainObject(p)) {
			if (UI.isEmptyObject(p)) {
				result = true;
			}
		}
		return result;
	}

	static URLParameters = {
		fromJson: function (jsonParam) {
			var url = Object.keys(jsonParam).map(function (k) {
				return encodeURIComponent(k) + '=' + encodeURIComponent(jsonParam[k])
			}).join('&');
			return url;
		}
	}

	static XMLHttp = {
		invokeMethodAsync: function (URL, jsonParam) {
			return new Promise(function (resolve, reject) {
				URL = `${URL}?${UI.URLParameters.fromJson(jsonParam)}`
				var xhr = new XMLHttpRequest();
				xhr.responseType = 'json';
				xhr.open("GET", URL, true);
				xhr.onload = function () {
					if (this.status >= 200 && this.status < 300) {
						resolve(xhr.response);
					} else {
						reject({
							status: this.status,
							statusText: xhr.statusText
						});
					}
				};
				xhr.onerror = function () {
					reject({
						status: this.status,
						statusText: xhr.statusText
					});
				};
				//if (opts.headers) {
				//    Object.keys(opts.headers).forEach(function (key) {
				//        xhr.setRequestHeader(key, opts.headers[key]);
				//    });
				//}
				xhr.send();
			});
		}
	};
}
